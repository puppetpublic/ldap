# This module installs the LDAP, PAM, and NSS support required to
# use an LDAP directory as the source for Posix account and group
# information.  If needed this module installs a local LDAP proxy
# server that uses a Kerberos bind to the central service and
# allows anonymous local access to the directory information.
#
# There are two steps require to implement Workgroup control:
#
#   1. Obtain access the directory data for the workgroup.  This
#      access is granted to any administrator for a workgroup and does
#      not require Data Owner approval.  See the ldap ikiwiki pages
#      for the details of creating the access group.  Once the access
#      group exists the ldap auth remctl command can be used to grant
#      access.  For example,
#
#      % remctl ldap-master ldap auth host/foo.stanford.edu accessgroup rra
#
#
#   2. Configure any hosts that need the access control.  That
#      is what this module does.
#
# In addition to controlling access to a system using data from the
# cn=Accounts branch of the directory, Posix group information can
# be pulled from the cn=groups branch of the directory.  Once the
# directory ACL structures are created a gidNumber needs to be
# assigned to the Workgroups that will also be used as Posix groups.
# The required command is:
#
#      % remctl ldap-master ldap group add --type=posix stem:group
#
# Examples:
#
#   To restrict access to a single Workgoup and to pull a single group
#   from the LDAP directory define account and group filters.
#
#      ldap::workgroup { 'anesthesia':
#        ensure              => 'present',
#        ldap_account_filter => '(&(objectClass=posixAccount)(suPrivilegeGroup=stanford:staff))',
#        ldap_group_filter   => '(&(objectclass=suPosixGroup)(cn=stanford:staff)',
#      }
#
#   To allow anyone in the workgroup stem access an LDAP filter does
#   not need to be specified.
#
#      ldap::workgroup { 'anesthesia': ensure => 'present' }
#
# Testing:
#
#   To make sure that the correct account information is being
#   return the getent command can be used.  For example:
#
#     % getent passwd whm
#     % getent group somegroup
#
#   On systems that have an ldap proxy installed a simple ldapsearch
#   can be used to verify anonymous directory connectivity.  For
#   example:
#
#     % ldapsearch -x
#
#   should return all of the posixAccount entries for the choosen
#   Workgroup stem.

define ldap::workgroup (
  $ensure              = 'present',
  $base_package        = 'ldapd',
  $ldap_host           = 'ldap.stanford.edu',
  $ldap_base           = 'dc=stanford,dc=edu',
  $ldap_account_filter = '(objectclass=posixAccount)',
  $ldap_group_filter   = '(objectclass=suPosixGroup)',
  $ldap_proxy          = 'NONE',
  $principal           = 'HOST',
  $tmpl_nslcd          = 'ldap/etc/nslcd.conf.erb',
  $tmpl_nsswitch       = 'NONE',
  $tmpl_pam_ldap       = 'ldap/ldapd-redhat/etc/pam_ldap.conf.erb',
  $tmpl_sssd           = 'ldap/sssd/etc/sssd/sssd.conf.erb'
) {

  case $operatingsystem {
    'RedHat', 'CentOS': {
      if $base_package != 'ldapd' {
        if $base_package != 'sssd' {
          fail ("Unknown base_package.  Must be 'sssd' or 'ldapd'.")
        }
      }
    }
    'Debian', 'Ubuntu': {
      if $base_package != 'ldapd' {
        fail ("Unknown base_package.  Much be 'ldapd'.")
      }
    }
    default: { fail('unrecognized operating system') }
  }

  case $ensure {
    'absent': {

      # Remove the packages and the configuration files directly
      # supporting the packages and update nsswitch.conf if needed.
      file { '/etc/nslcd.conf': ensure => absent }

      exec { 'remove ldap from nsswitch.conf':
        command  => "perl -pe 's/ldap//' -i /etc/nsswitch.conf",
        provider => shell,
        onlyif   => 'grep ldap /etc/nsswitch.conf',
      }

      case $operatingsystem {
        'RedHat', 'CentOS': {
          file { '/etc/pam_ldap.conf': ensure => absent }
          case $base_package {
            'ldapd': {
              package {
                'nss-pam-ldapd': ensure => absent;
                'pam_ldap':      ensure => absent;
              }
            }
            'sssd': {
              package { 'sssd': ensure => absent }
            }
          }
        }
        'Debian', 'Ubuntu': {
          package {
            'libpam-ldapd': ensure => absent;
            'libnss-ldapd': ensure => absent;
          }
        }
      }
    }

    default: {

      # Turn off user management
      include user::managed::disabled

      case $operatingsystem {
        'RedHat', 'CentOS': {
          include ldap::workgroup_redhat
          case $base_package {
            'sssd': {
              # sssd has support for kerberos binds to the directory,
              # but it does not support mapping attributes.
              case $tmpl_nsswitch {
                'NONE':  { $nsswitch = 'ldap/sssd/etc/nsswitch.conf.erb' }
                default: { $nsswitch = $tmpl_nsswitch                    }
              }
              file {
                '/etc/sssd':
                  owner   => 'root',
                  group   => 'root',
                  mode    => 700,
                  ensure  => directory;
                '/etc/sssd/sssd.conf':
                  owner   => 'root',
                  group   => 'root',
                  mode    => 600,
                  content => template($tmpl_sssd),
                  require => File['/etc/sssd'];
                '/etc/nsswitch.conf':
                  mode    => 644,
                  content => template($nsswitch),
                  require => Package['sssd'];
              }
              package {
                'sssd':
                  ensure  => present,
                  require => File['/etc/sssd/sssd.conf'];
              }
            }
            'ldapd': {
              # The version of pam-nss-ldapd available on RHEL6
              # systems doesn't support kerberos binds to the
              # directory and requires an ldap proxy.
              service { 'nslcd':
                ensure    => running,
                enable    => true,
                restart   => '/etc/init.d/nslcd force-reload',
                subscribe => File['/etc/nslcd.conf'],
                require   => Service['slapd'],
              }
              exec {'nslcd refresh':
                command     => '/etc/init.d/nslcd force-reload',
                path        => ['/bin','/usr/sbin'],
                refreshonly => true,
                require     => File['/etc/nslcd.conf'],
                returns     => 0,
                logoutput   => true,
              }
              $thisUID = 'nslcd'
              $thisGID = 'ldap'
              case $tmpl_nsswitch {
                'NONE':  {
                  $nsswitch = 'ldap/ldapd-redhat/etc/nsswitch.conf.erb'
                }
                default: {
                  $nsswitch = $tmpl_nsswitch
                }
              }
              # Create a slapd proxy to allow anonymous local searches.
              $useProxy = true
              ldap::proxy {$::hostname:
                ensure       => present,
                principal    => $principal,
                ldapHost     => $ldap_host,
                ldapConfBase => $ldap_base
              }
              package {
                'nss-pam-ldapd': ensure => present;
                'pam_ldap':      ensure => present;
              }
              file { '/etc/pam_ldap.conf':
                content => template($tmpl_pam_ldap),
                require => Package['pam_ldap'],
              }
              file { '/etc/nslcd.conf':
                content => template($tmpl_nslcd),
                notify  => Exec['nslcd refresh'],
                require => Package['nss-pam-ldapd'],
              }
              file { '/etc/nsswitch.conf':
                content => template($nsswitch),
              }
            }
          }
        }

        # Later versions of debian and ubuntu support pam/nss kerberos
        # binds to the directory which obviates the need for a slapd
        # proxy server.  Once can be used it desired, but recommended
        # practice is to not use a proxy.
        'Debian', 'Ubuntu': {
          $thisUID = 'nslcd'
          $thisGID = 'nslcd'
          package {
            'libpam-ldapd': ensure => present;
            'libnss-ldapd': ensure => present;
          }
          case $tmpl_nsswitch {
            'NONE':  { $nsswitch = 'ldap/ldapd-debian/etc/nsswitch.conf.erb' }
            default: { $nsswitch = $tmpl_nsswitch                }
          }

          # When the nslcd.conf file changes reload the changes.
          exec {'nslcd refresh':
            command     => '/etc/init.d/nslcd force-reload',
            path        => ['/bin','/usr/sbin'],
            refreshonly => true,
            require     => File['/etc/nslcd.conf'],
            returns     => 0,
            logoutput   => true,
          }

          if $ldap_proxy == 'NONE' {
            $useProxy = false
            $ldapConfBase = $ldap_base
            file {
              '/etc/ldap':
                ensure => directory,
                mode   => '0755';
              '/etc/ldap/ldap.conf':
                mode    => '0644',
                content => template('ldap/etc/ldap/ldap.conf.erb'),
                require => File['/etc/ldap'];
              '/var/run/nslcd':
                ensure => directory,
                mode   => '0755',
                owner  => $thisUID,
                group  => $thisGID,
                require => Package['libpam-ldapd','libnss-ldapd'];
            }
            package {
              'libsasl2-modules-gssapi-mit':
                ensure  => installed;
              'ldap-utils':
                ensure  => installed,
                require => File['/etc/ldap'];
            }
          } else {
            $useProxy = true
            ldap::proxy {$::hostname:
              ensure       => present,
              principal    => $principal,
              ldapHost     => $ldap_host,
              ldapConfBase => $ldap_base
            }
          }
          file { '/etc/nslcd.conf':
            content => template($tmpl_nslcd),
            notify  => Exec['nslcd refresh'],
            require => Package['libnss-ldapd'],
          }
          file { '/etc/nsswitch.conf':
            content => template($nsswitch),
          }
        }
        default: { fail('unrecognized operating system') }
      }
    }
  }
}

# Over ride pam configuration on redhat systems

class ldap::workgroup_redhat inherits base::pam::redhat {
  File['/etc/pam.d/system-auth'] { target => '/etc/pam.d/system-auth-ldap' }
  file {
    '/etc/pam.d/system-auth-ldap':
      mode   => 644,
      source => 'puppet:///modules/base/pam/etc/pam.d/system-auth-ldap',
  }
}
