# Class that deploys the current Stanford LDAP schema definitions.
#

class ldap::schema {

  # Set variables with paths for debian or rhel or rhel5
  case $::operatingsystem {
    'RedHat': {
      $ldapPath = '/etc/openldap'
      if $::lsbmajdistrelease == 5 {
        $schema_dir = "schema-rhel5"
      } else {
        $schema_dir = 'schema'
      }
    }
    default:  {
      $ldapPath   = '/etc/ldap'
      $schema_dir = 'schema'
    }
  }

  file {
    "$ldapPath/schema":
      source  => "puppet:///modules/ldap/$schema_dir",
      recurse => true;
  }
}
