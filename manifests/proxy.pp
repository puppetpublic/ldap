##############################################################################
# LDAP Proxy
##############################################################################
#
# LDAP proxy configuration for access the central LDAP service.  The
# proxy makes a kerberos bind to the central server and local
# applications can bind anonymously.
#
# The default Kerberos principal used to access the LDAP directory is
# the host keytab stored in /etc/krb5.keytab.  If any other principal is
# specified the keytab will be downloaded using wallet by this
# procedure.
#
# This define takes care of downloading the correct packages,
# installing the ldap configuration, downloading the keytab, and
# setting up the supervise or systemd to keep the keytab refreshed.
# What is required to use the service is to get dataowner approval to
# access the directory, and have the ACLs on the central LDAP service
# modified to allow the access.
#
# The systemd_k5start shared module is required in order to use this define
# on systems that use systemd.
#
# Example:
#
#  ldap::proxy { $::fqdn_lc: ensure => present }
#

define ldap::proxy (
  $ensure       = 'present',
  $debuglevel   = 'stats',
  $sizelimit    = 'unlimited',
  $principal    = 'HOST',
  $ldapHost     = 'ldap.stanford.edu',
  $ldapConfBase = 'dc=stanford,dc=edu',
  $ldapUser     = 'ldap',
  $ldapGroup    = 'ldap',
  $keytab       = 'NONE',
  $tgtMaint     = 'true',
  $useRootDN    = 'false',
) {

  # Set variables first so that we can process ensure => absent.
  case $::osfamily {
    'RedHat': { $ldapPath = '/etc/openldap' }
    default:  { $ldapPath = '/etc/ldap'     }
  }

  case $ensure {
    present: {
      include ldap::schema
      case $::osfamily {
        'RedHat': { include ldap::proxy::redhat }
        'Debian': { include ldap::proxy::debian }
        default:  { fail('ldap::proxy does not support this OS family') }
      }
      file {
        $ldapPath:
          ensure => directory,
          mode   => '0755';
        "$ldapPath/ldap.conf":
          mode    => '0644',
          content => template('ldap/proxy/ldap.conf.erb');
        "$ldapPath/slapd.d":
          ensure  => absent,
          recurse => true,
          force   => true;
        "$ldapPath/slapd.conf":
          content => template('ldap/proxy/slapd-proxy.conf.erb');
      }
      case $keytab {
        'NONE': {
          case $principal {
            'HOST': {
              $keytabFile = '/etc/krb5.keytab'
            }
            default: {
              $keytabFile = "$ldapPath/slapd-proxy.keytab"
              base::wallet { "$principal":
                ensure  => present,
                owner   => $ldapUser,
                group   => $ldapGroup,
                path    => $keytabFile,
              }
            }
          }
        } # Done handling $keytab == 'NONE'
        default: {
          $keytabFile = $keytab
        }
      } # Done looking at $keytab
      if $tgtMaint == 'true' {
        # If we have an OS that runs systemd, then make sure we use the systemd
        # unit instead of daemontools.  Also, make sure the old systemd unit
        # is removed.
        if    (($::operatingsystem == 'Debian') and ($::lsbmajdistrelease >= 8))
           or (($::operatingsystem == 'Ubuntu') and ($::lsbmajdistrelease >= 15))
           or (    (   ($::operatingsystem == 'RedHat')
                    or ($::operatingsystem == 'CentOS'))
               and ($::lsbmajdistrelease >= 7)
           )
        {
          base::daemontools::supervise { 'ldap-proxy': ensure => absent }
          file { '/etc/systemd/system/slapd-tgt.service': ensure => absent }
          systemd_k5start{ 'slapd-tgt':
            description  => 'Maintain ticket for the LDAP proxy',
            ticket_file  => '/var/run/ldap-proxy/ldap-proxy.tgt',
            keytab       => $keytabFile,
            owner        => $ldapUser,
            group        => $ldapGroup,
            mode         => '0660',
            start_before => 'slapd.service',
            require      => File['/etc/systemd/system/slapd-tgt.service'],
          }
          service { 'slapd-tgt':
            ensure  => 'running',
            enable  => true,
            require => Systemd_K5start['slapd-tgt'],
          }
        } else {
          # Systems that don't use systemd instead use daemontools.
          base::daemontools::supervise { 'ldap-proxy':
            ensure  => present,
            content => template('ldap/proxy/ldap-proxy.run.erb'),
          }
        }
      } # Done handling $tgtMaint == true
    } # Done handling $ensure == present
    absent: {
      if $tgtMaint == 'true' {
        # On systemd systems, make sure we remove the systemd services
        if    (($::operatingsystem == 'Debian') and ($::lsbmajdistrelease >= 8))
           or (($::operatingsystem == 'Ubuntu') and ($::lsbmajdistrelease >= 15))
           or (    (   ($::operatingsystem == 'RedHat')
                    or ($::operatingsystem == 'CentOS'))
               and ($::lsbmajdistrelease >= 7)
           )
        {
          file { '/etc/systemd/system/slapd-tgt.service': ensure => absent }
          systemd_k5start{ 'slapd-tgt':
            ticket_file => '/var/run/ldap-proxy/ldap-proxy.tgt',
            ensure      => absent,
          }
        }
        # For all systems, make sure the supervise is also removed.
        base::daemontools::supervise { 'ldap-proxy': ensure => absent }
      }
    } # Done handling $ensure == absent
    default: {
      fail('Invalid ensure specified for ldap::proxy')
    }
  } # Done looking at $ensure
}

##############################################################################
# Red Hat Specifics
##############################################################################

class ldap::proxy::redhat {

  file {
    '/etc/sysconfig/ldap':
      mode    => 0664,
      content => template('ldap/proxy/sysconfig-ldap.erb');
    '/var/run/ldap':
      ensure  => directory,
      owner   => 'ldap';
  }

  package {
    'cyrus-sasl-gssapi':
      ensure  => installed;
    'openldap-clients':
      ensure  => installed,
      require => [ File['/etc/openldap/schema'],
                   File['/etc/openldap/slapd.conf'] ];
    'openldap-servers':
      ensure  => installed,
      require => [ File['/etc/openldap/schema'],
                   File['/etc/openldap/slapd.conf'] ];
  }

  service { 'slapd':
    ensure    => running,
    enable    => true,
    subscribe => [ File['/etc/sysconfig/ldap'],
                   File['/etc/openldap/schema'],
                   File['/etc/openldap/slapd.conf'],
                   File['/var/run/ldap'] ],
  }
}

##############################################################################
# Debian Specifics
##############################################################################

class ldap::proxy::debian {
  include user::ldap

  file {
    '/etc/default/slapd':
      mode    => '0644',
      content => template('ldap/proxy/default-slapd.erb');
    '/var/run/ldap-proxy':
      ensure  => directory,
      owner   => 'ldap';
  }

  preseed_debconf { 'preseed-slapd':
    source => 'puppet:///modules/ldap/etc/apt/preseed-slapd',
    answer => 'slapd.*slapd/no_configuration.*boolean.*true',
  }

  package { 'slapd':
    ensure  => installed,
    require => [ Preseed_debconf['preseed-slapd'],
                 File['/etc/default/slapd'],
                 File['/etc/ldap/schema'],
                 File['/etc/ldap/slapd.conf'] ];
  }

  package {
    'libsasl2-modules-gssapi-mit':
      ensure  => installed;
    'ldap-utils':
      ensure  => installed,
      require => File['/etc/ldap/schema'];
  }

  service { 'slapd':
    ensure     => running,
    hasstatus  => false,
    status     => 'pidof slapd',
    hasrestart => true,
    subscribe  => [ File['/etc/default/slapd'],
                    File['/etc/ldap/schema'],
                    File['/etc/ldap/slapd.conf'] ],
  }
}
