#############################################################################
# Original slapd_proxy module - depreciated in favor of ldap::proxy
##############################################################################

class ldap::slapd_proxy {
    include base::limits

    service { 'slapd':
        ensure     => running,
        hasstatus  => false,
        status     => 'pidof slapd',
        hasrestart => true, 
        subscribe  => [ File['/etc/default/slapd'], 
                        File['/etc/ldap/schema'],
                        File['/etc/ldap/slapd.conf'],
                        File['/etc/ldap/conf.d/slapd_custom.conf'] ],
    }

    base::limits::conf { 
        'openldap-soft':
            domain => openldap,
            type   => soft,
            item   => nofile,
            value  =>  4096;
        'openldap-hard':
            domain => openldap,
            type => hard,
            item => nofile,
            value => 10240;
    }

    file {
        '/etc/ldap':
            ensure => directory;
        '/etc/ldap/conf.d': 
            ensure => directory;
        '/etc/ldap/slapd.conf': 
          source  => 'puppet:///modules/ldap/slapd_proxy/etc/ldap/slapd.conf';
        '/etc/ldap/schema':
          source  => 'puppet:///modules/ldap/schema',
          recurse => true;
    }

    ldap::slapd_proxy::config { $::fqdn_lc:
        runasuser  => 'openldap',
        runasgroup => 'openldap',
    }  

    preseed_debconf { 'preseed-slapd':
        source => 'puppet:///modules/ldap/slapd_proxy/etc/apt/preseed-slapd',
        answer => 'slapd.*slapd/no_configuration.*boolean.*true',
    }

    package { 'slapd':
        ensure => installed,
        require => [ Preseed_debconf['preseed-slapd'],
                     File['/etc/default/slapd'],
                     File['/etc/ldap/schema'],
                     File['/etc/ldap/slapd.conf'],
                     File['/etc/ldap/conf.d/slapd_custom.conf'] ];
    }
}

##############################################################################
# slapd proxy configuration
##############################################################################
#
# Usage example;
#     ldap::slapd_proxy::config{ $::fqdn_lc:
#         bindas => 'smtp',
#         type   => 'host', 
#     }
#
# Configuration for different applications
define ldap::slapd_proxy::config(
  $ensure     = 'present',
  $runasuser  = 'openldap',
  $runasgroup = 'openldap',
  $type       = 'service',
  $sizelimit  = '25',
  $ldaphost   = 'ldap.stanford.edu',
  $debuglevel = '256',
  $keytabpath = NOPATH,
  $bindas
) {

  case $ensure {
    absent: { 
      # Place holder
    }
    present: {
      # The slapd will run as this user and this group
      user { $runasuser: 
        groups => $runasgroup;
      }
      
      # Service principal for GSSAPI authentication
      if ($type == 'host' ) {
        $keytab = "$bindas/${::fqdn_lc}"
      } elsif $type == 'service' {
        $keytab = "service/$bindas"
      } else {
        fail('Binding principal type should be either host or service')
      }
      
      if $keytabpath == 'NOPATH' {
        $keytabloc = "/etc/${bindas}.keytab"
      } else {
        $keytabloc = "$keytabpath"
      }
      
      base::wallet { "$keytab":
        path    => $keytabloc,
        owner   => 'root',
        group   => $runasgroup,
        mode    => 640,
        ensure  => present,
        require => Package['slapd'];
      }
      
      base::daemontools::supervise { 'slapd-k5':
        ensure  => present,
        content => template('ldap/slapd_proxy/slapd-k5.run.erb'),
        require => File[ $keytabloc ];
      }
      
      file { '/etc/default/slapd':
        content => template('ldap/slapd_proxy/default-slapd.erb'),
      }
      
      file { '/etc/ldap/conf.d/slapd_custom.conf':
        content => template('ldap/slapd_proxy/slapd_custom.conf.erb'),
      }
    }
    default: { crit "Invalid ensure value: $ensure" }
  }
}
