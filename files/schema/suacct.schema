# Accounts tree schema 4/03/02

attributetype (StanfordLDAPattributeType:40 NAME ( 'suEntryStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:41 NAME ( 'suCreateAPI' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:42 NAME ( 'suCreateAgent' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:3 NAME 'suOperational'
    AUXILIARY
    DESC 'Stanford University Operational Housekeeping'
        MUST ( suEntryStatus $ suCreateAgent $ suCreateAPI )
    )

attributetype (StanfordLDAPattributeType:100 NAME ( 'suAccountStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:200 NAME ( 'suName' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:203 NAME ( 'suNameLF' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:300 NAME ( 'suDescription' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype (StanfordLDAPattributeType:400 NAME ( 'suService' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype (StanfordLDAPattributeType:500 NAME ( 'suIdentifies' )
    EQUALITY distinguishedNameMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.12 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:61 NAME ( 'suAdministratorDN' )
    EQUALITY distinguishedNameMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.12 )

objectclass (StanfordLDAPobjectClass:300 NAME 'suAccount'
    DESC 'Stanford University Account'
    SUP ( account )
    STRUCTURAL
        MUST ( uid $ suName $ suNameLF $ suAccountStatus )
        MAY ( owner $ suDescription $ suService $ suIdentifies $ suAdministratorDN )
    )

attributetype (StanfordLDAPattributeType:101 NAME ( 'suKerberosStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:201 NAME ( 'suKrb4Name' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

objectclass (StanfordLDAPobjectClass:301 NAME 'suKerberosService' AUXILIARY
    DESC 'Kerberos Service'
        MUST ( suKerberosStatus )
        MAY ( krb5PrincipalName $ suKrb4Name )
    )


attributetype (StanfordLDAPattributeType:109 NAME ( 'suCGIStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:606 NAME ( 'suCGIUid' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:9001 NAME ( 'suCGIAfsDirectory' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:309 NAME 'suCGIService' AUXILIARY
    DESC 'CGI Service'
        MUST ( suCGIStatus $ suCGIUid $ suCGIAfsDirectory )
    )

attributetype (StanfordLDAPattributeType:110 NAME ( 'suCalendarStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:312 NAME 'suCalendarService' AUXILIARY
    DESC 'Calendar Service'
        MUST ( suCalendarStatus )
    )

attributetype (StanfordLDAPattributeType:102 NAME ( 'suSeasStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:7 NAME ( 'suSeasSunetID' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{256} )

attributetype ( StanfordLDAPattributeType:76 NAME 'suSeasSunetIDPreferred'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{256} )

attributetype (StanfordLDAPattributeType:700 NAME ( 'suSeasLocal' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:701 NAME ( 'suSeasForward' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype (StanfordLDAPattributeType:800 NAME ( 'suMailDrop' )
    EQUALITY caseIgnoreIA5Match
    SUBSTR caseIgnoreIA5SubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{256} )

attributetype (StanfordLDAPattributeType:1000 NAME ( 'suSeasUriRouteTo' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:302 NAME 'suSeasService' AUXILIARY
    DESC 'Stanford Electronic Alias Service'
        MUST ( suSeasStatus $ suSeasSunetID $ suMailDrop )
        MAY ( suSeasUriRouteTo $ suSeasLocal $ suSeasForward
              $ suSeasSunetIDPreferred)
    )

attributetype (StanfordLDAPattributeType:103 NAME ( 'suLelandStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:303 NAME 'suLelandService' AUXILIARY
    DESC 'Leland login account services'
        MUST ( suLelandStatus )
    )

attributetype (StanfordLDAPattributeType:104 NAME ( 'suAfsStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:9000 NAME ( 'suAfsHomeDirectory' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:304 NAME 'suAfsService' AUXILIARY
    DESC 'AFS Service'
        MUST ( suAfsStatus )
        MAY ( suAfsHomeDirectory )
    )

attributetype (StanfordLDAPattributeType:105 NAME ( 'suPtsStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:601 NAME ( 'suPtsUid' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{256} SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:305 NAME 'suPtsService' AUXILIARY
    DESC 'PTS Service'
        MUST ( suPtsStatus )
        MAY ( suPtsUid )
    )

attributetype (StanfordLDAPattributeType:106 NAME ( 'suEmailStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:1001 NAME ( 'suEmailQuota' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:1002 NAME ( 'suEmailAccountType' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:1003 NAME ( 'suEmailAdmin' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

objectclass (StanfordLDAPobjectClass:306 NAME 'suEmailService' AUXILIARY
    DESC 'Email Service'
        MAY ( suEmailStatus $ suEmailQuota $ suEmailAccountType $ suEmailAdmin )
    )

attributetype (StanfordLDAPattributeType:107 NAME ( 'suDialinStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:307 NAME 'suDialinService' AUXILIARY
    DESC 'Dial-In Service'
        MUST ( suDialinStatus )
    )

attributetype (StanfordLDAPattributeType:108 NAME ( 'suAutoreplyStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:702 NAME ( 'suAutoreplyForward' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:703 NAME ( 'suAutoreplySubj' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:704 NAME ( 'suAutoreplyMsg' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:705 NAME ( 'suAutoreplyAlias' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype (StanfordLDAPattributeType:706 NAME ( 'suAutoreplyStart' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype (StanfordLDAPattributeType:707 NAME ( 'suAutoreplyStop' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

objectclass (StanfordLDAPobjectClass:308 NAME 'suAutoreplyService' AUXILIARY
    DESC 'Email Auto-Reply Feature'
        MUST ( suAutoreplyStatus $ suAutoreplyForward $ suAutoreplySubj $ suAutoreplyMsg )
        MAY ( suAutoreplyAlias $ suAutoreplyStart $ suAutoreplyStop )
    )

attributetype (StanfordLDAPattributeType:111 NAME ( 'suEntitlementStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:550 NAME ( 'suEntitlementName' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

objectclass (StanfordLDAPobjectClass:313 NAME 'suEntitlementService' AUXILIARY
    DESC 'Account Entitlements'
        MUST ( suEntitlementStatus )
        MAY ( suEntitlementName )
    )

attributetype (StanfordLDAPattributeType:112 NAME ( 'suGuestStatus' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:205 NAME ( 'suGuestName' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:607 NAME ( 'suGuestUuid' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{256} SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:315 NAME ( 'suGuestAltLogin' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

objectclass (StanfordLDAPobjectClass:314 NAME 'suGuestService'
        AUXILIARY
        DESC 'Guest Account Services'
        MUST ( suGuestStatus $ suGuestName $ suGuestUuid )
        MAY ( suGuestAltLogin )
    )
