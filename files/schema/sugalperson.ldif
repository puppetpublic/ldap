# The objectClass suGALperson that contains all the suGAL* attributes has "top" as
# its parent class.

dn: cn=sugalperson,cn=schema,cn=config
objectClass: olcSchemaConfig
olcAttributeTypes: ( StanfordLDAPattributeType:3050
  NAME ( 'suGALc' 'suGALcountryName' )
  DESC 'Stanford privacy controlled FC4519: two-letter ISO-3166 country code (suVisibHomeAddress)'
  SUP name SYNTAX 1.3.6.1.4.1.1466.115.121.1.11
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3051
  NAME 'suGALcn'
  DESC 'Privacy controlled cn (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3052
  NAME 'suGALdisplayName'
  DESC 'Privacy controlled displayName (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3053
  NAME 'suGALsuEmailPager'
  DESC 'Privacy controlled suEmailPager (suVisibPagerEmail)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3054
  NAME ( 'suGALfacsimileTelephoneNumber' 'suGALfax' )
  DESC 'Stanford privacy controlled RFC2256: Facsimile (Fax) Telephone Number (suVisibFacsimileTelephoneNumber)'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.22 )
olcAttributeTypes: ( StanfordLDAPattributeType:3055
  NAME 'suGALgenerationQualifier'
  DESC 'Privacy controlled generationQualifier (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3056
  NAME 'suGALgivenName'
  DESC 'Privacy controlled givenName (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3057
  NAME ( 'suGALhomePhone' 'suGALhomeTelephoneNumber' )
  DESC 'Stanford privacy controlled RFC1274: home telephone number (suVisibHomeAddress)'
  EQUALITY telephoneNumberMatch
  SUBSTR telephoneNumberSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )
olcAttributeTypes: ( StanfordLDAPattributeType:3058
  NAME 'suGALhomePostalAddress'
  DESC 'Stanford privacy controlled RFC1274: home postal address (suVisibHomeAddress)'
  EQUALITY caseIgnoreListMatch
  SUBSTR caseIgnoreListSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.41 )
olcAttributeTypes:  ( StanfordLDAPattributeType:3059
  NAME ( 'suGALl' 'suGALlocalityName' )
  DESC 'Stanford privacy controlled RFC2256: locality which this object resides in (suVisibHomeAddress)'
  SUP name )
olcAttributeTypes: ( StanfordLDAPattributeType:3060
  NAME 'suGALlabeledURI'
  DESC 'Privacy controlled labeledURI (suVisibHomePage)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3061
  NAME 'suGALmail'
  DESC 'Privacy controlled mail (suVisibEmail)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3062
  NAME ( 'suGALmobile' 'suGALmobileTelephoneNumber' )
  DESC 'Stanford privacy controlled RFC1274: mobile telephone number (suVisibMobilePhone)'
  EQUALITY telephoneNumberMatch
  SUBSTR telephoneNumberSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )
olcAttributeTypes: ( StanfordLDAPattributeType:3063
  NAME ( 'suGALpager' 'suGALpagerTelephoneNumber' )
  DESC 'Stanford privacy controlled RFC1274: pager telephone number (suVisibPagerPhone)'
  EQUALITY telephoneNumberMatch
  SUBSTR telephoneNumberSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )
olcAttributeTypes: ( StanfordLDAPattributeType:3064
  NAME 'suGALpersonalTitle'
  DESC 'Privacy controlled personalTitle (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3065
  NAME 'suGALpostalAddress'
  DESC 'Stanford privacy controlled RFC2256: postal address (suVisibHomeAddress)'
  EQUALITY caseIgnoreListMatch
  SUBSTR caseIgnoreListSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.41 )
olcAttributeTypes: ( StanfordLDAPattributeType:3066
  NAME 'suGALpostalCode'
  DESC 'Stanford privacy controlled RFC2256: postal code (suVisibHomeAddress)'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{40} )
olcAttributeTypes: ( StanfordLDAPattributeType:3067
  NAME 'suGALsn'
  DESC 'Privacy controlled sn (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3068
  NAME 'suGALsuDisplayNameFirst'
  DESC 'Privacy controlled suDisplayNameFirst (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3069
  NAME 'suGALsuDisplayNameLast'
  DESC 'Privacy controlled suDisplayNameLast (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3070
  NAME 'suGALsuDisplayNameLF'
  DESC 'Privacy controlled suDisplayNameLF (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3071
  NAME 'suGALsuDisplayNameMiddle'
  DESC 'Privacy controlled suDisplayNameMiddle (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3072
  NAME 'suGALsuDisplayNamePrefix'
  DESC 'Privacy controlled suDisplayNamePrefix (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3073
  NAME 'suGALsuDisplayNameSuffix'
  DESC 'Privacy controlled suDisplayNameSuffix (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3074
  NAME 'suGALsuFacultyAppointment'
  DESC 'Privacy controlled suFacultyAppointment (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3075
  NAME 'suGALsuFacultyAppointmentShort'
  DESC 'Privacy controlled suFacultyAppointmentShort (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3076
  NAME 'suGALsuLocalAddress'
  DESC 'Privacy controlled suLocalAddress (suVisibLocalAddress)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3077
  NAME 'suGALsuLocalPhone'
  DESC 'Privacy controlled suLocalPhone (suVisibLocalPhone)'
  EQUALITY telephoneNumberMatch
  SUBSTR telephoneNumberSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.50
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3078
  NAME 'suGALsuMailAddress'
  DESC 'Privacy controlled suMailAddress (suVisibMailAddress)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3079
  NAME 'suGALsuOtherName'
  DESC 'Privacy controlled suOtherName (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3080
  NAME 'suGALsuPermanentAddress'
  DESC 'Privacy controlled suPermanent Address (suVisibPermanentAddress)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3081
  NAME 'suGALsuPermanentPhone'
  DESC 'Privacy controlled suPermanentPhone (suVisibPermanentPhone)'
  EQUALITY telephoneNumberMatch
  SUBSTR telephoneNumberSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.50
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3082
  NAME 'suGALsuResidenceRoom'
  DESC 'Privacy controlled suResidenceRoom (suVisibLocalAddress)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3083
  NAME 'suGALsuRegID'
  DESC 'Privacy controlled suRegID (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3084
  NAME 'suGALsuResidencePhone'
  DESC 'Privacy controlled suResidencePhone (suVisibLocalPhone)'
  EQUALITY telephoneNumberMatch
  SUBSTR telephoneNumberSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.50
  SINGLE-VALUE )
olcAttributeTypes: ( StanfordLDAPattributeType:3085
  NAME 'suGALsuSearchID'
  DESC 'Privacy controlled suSearchID (suVisibSunetID)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3086
  NAME ( 'suGALst' 'suGALstateOrProvinceName' )
  DESC 'Stanford privacy controlled RFC2256: state or province which this object resides in (suVisibHomeAddress)'
  SUP name )
olcAttributeTypes: ( StanfordLDAPattributeType:3087
  NAME ( 'suGALstreet' 'suGALstreetAddress' )
  DESC 'Stanford privacy controlled RFC2256: street address of this object (suVisibHomeAddress)'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{128} )
olcAttributeTypes: ( StanfordLDAPattributeType:3088
  NAME 'suGALsuSunetID'
  DESC 'Privacy controlled suSunetID (suVisibSunetID)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3089
  NAME 'suGALtelephoneNumber'
  DESC 'Stanford privacy controlled RFC2256: Telephone Number (suVisibTelephoneNumber)'
  EQUALITY telephoneNumberMatch
  SUBSTR telephoneNumberSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.50{32} )
olcAttributeTypes: ( StanfordLDAPattributeType:3090
  NAME 'suGALuid'
  DESC 'Privacy controlled uid (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )
olcAttributeTypes: ( StanfordLDAPattributeType:3091
  NAME 'suGALsuUniqueIdentifier'
  DESC 'Privacy controlled suUniqueIdentifier (suVisibIdentity)'
  EQUALITY caseIgnoreMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )
olcObjectClasses: ( StanfordLDAPobjectClass:462
  NAME 'suGALperson'
  DESC 'Privacy controlled suPerson attributes'
  SUP top
  AUXILIARY
  MUST ( suRegID )
  MAY (suGALc $ suGALcn $ suGALdisplayName $ suGALsuEmailPager $
       suGALfacsimileTelephoneNumber $ suGALgenerationQualifier $
       suGALgivenName $ suGALhomePhone $ suGALhomePostalAddress $
       suGALl $ suGALlabeledURI $ suGALmail $ suGALmobile $
       suGALpager $ suGALpersonalTitle $ suGALpostalAddress $
       suGALpostalCode $ suGALsn $ suGALsuDisplayNameFirst $
       suGALsuDisplayNameLast $ suGALsuDisplayNameLF $
       suGALsuDisplayNameMiddle $ suGALsuDisplayNamePrefix $
       suGALsuDisplayNameSuffix $ suGALsuFacultyAppointment $
       suGALsuFacultyAppointmentShort $ suGALsuLocalAddress $
       suGALsuLocalPhone $ suGALsuMailAddress $ suGALsuOtherName $
       suGALsuPermanentAddress $ suGALsuPermanentPhone $
       suGALsuResidenceRoom $ suGALsuRegID $ suGALsuResidencePhone $
       suGALsuSearchID $ suGALst $ suGALstreet $ suGALsuSunetID $
       suGALtelephoneNumber $ suGALuid $ suGALsuUniqueIdentifier ) )

