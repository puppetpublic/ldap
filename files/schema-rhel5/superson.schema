# Abstract/structural classes for cn=People, dc=Stanford, dn=EDU

attributetype ( StanfordLDAPattributeType:1
    NAME ( 'suRegID' )
    EQUALITY caseIgnoreIA5Match
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:2
    NAME ( 'suRegisteredName' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:3
    NAME ( 'suRegisteredNameLF' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4
    NAME ( 'suDisplayNameLF' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5
    NAME ( 'suOtherName' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} )

attributetype ( StanfordLDAPattributeType:6
    NAME ( 'suSunetID' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:8
    NAME ( 'suGeneralID' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:9
    NAME ( 'suDisplayAffiliation' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:11
    NAME ( 'suEmailPager' )
    EQUALITY caseIgnoreIA5Match
    SUBSTR caseIgnoreIA5SubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{256} SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:13
    NAME ( 'suStanfordEndDate' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:14
    NAME ( 'suAffiliation' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:15
    NAME ( 'suMailCode' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:17
    NAME ( 'suFacultyAppointment' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:18
    NAME ( 'suUnivID' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:19
    NAME ( 'suPrivilegeGroup' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:20
    NAME ( 'suSN' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:21
    NAME ( 'suGivenName' 'suGN' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:22
    NAME ( 'suCN' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:23
    NAME ( 'suDisplayNameFirst' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:24
    NAME ( 'suDisplayNameMiddle' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:25
    NAME ( 'suDisplayNameLast' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:26
    NAME ( 'suDisplayNamePrefix' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:27
    NAME ( 'suDisplayNameSuffix' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:28
    NAME ( 'suFacultyAppointmentShort' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:30
    NAME ( 'suUniqueIdentifier' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:60
    NAME ( 'suGALSyncDate' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:63
    NAME ( 'suStudentType' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:64
    NAME ( 'suOU' 'suOrganizationalUnitName' )
    DESC 'The Stanford organizational units this object belongs to'
    SUP name )

attributetype ( StanfordLDAPattributeType:65
    NAME ( 'suAffiliationType' )
    DESC 'Descriptive classification of an affiliation'
    SUP name )

attributetype ( StanfordLDAPattributeType:66
    NAME ( 'suDepartment' )
    DESC 'Stanford Department Name'
    SUP name )

attributetype ( StanfordLDAPattributeType:67
    NAME ( 'suInternalPager' )
    DESC 'Stanford Internal Pager Number'
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:69
    NAME ( 'suAffilJobCode' )
    DESC 'Employee job category'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:70
    NAME ( 'suAffilJobDescription' )
    DESC 'Employee job description'
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:71
    NAME ( 'suAffilStandardHours' )
    DESC 'The stanford number of hours in a work week'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:72
    NAME ( 'suJobCode' )
    DESC 'Employee job category'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:73
    NAME ( 'suJobDescription' )
    DESC 'Employee job description'
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:74
    NAME ( 'suStandardHours' )
    DESC 'The standard number of hours in a work week'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:75
    NAME ( 'suStanfordStartDate' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:68
    NAME ( 'suRelationship' )
    DESC 'Relationship of a person to Stanford'
    SUP name )

attributetype ( StanfordLDAPattributeType:202
    NAME ( 'suResidenceName' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:204
    NAME ( 'suPrimaryOrganizationName' )
    DESC '30 character length name'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:301
    NAME ( 'suProfile' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:602
    NAME ( 'suPrimaryOrganizationID' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype (StanfordLDAPattributeType:603
    NAME ( 'suCardNumber' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:604
    NAME ( 'suSearchID' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype (StanfordLDAPattributeType:605
    NAME ( 'suProxyCardNumber' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:801
    NAME ( 'suResidenceCode' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:802
    NAME ( 'suResidenceTSO' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:803
    NAME ( 'suADPrimaryEmail' )
    EQUALITY caseIgnoreIA5Match
    SUBSTR caseIgnoreIA5SubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{256} )

attributetype ( StanfordLDAPattributeType:900
    NAME ( 'suVisibEmail' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:901
    NAME ( 'suVisibHomeAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:902
    NAME ( 'suVisibHomePage' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:903
    NAME ( 'suVisibHomePhone' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:904
    NAME ( 'suVisibIdentity' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:905
    NAME ( 'suVisibFacsimileTelephoneNumber' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:906
    NAME ( 'suVisibMobilePhone' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:907
    NAME ( 'suVisibPagerEmail' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:908
    NAME ( 'suVisibPagerPhone' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:909
    NAME ( 'suVisibProfile' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:910
    NAME ( 'suVisibSunetID' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:911
    NAME ( 'suVisibAffiliation1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:912
    NAME ( 'suVisibAffiliation2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:913
    NAME ( 'suVisibAffiliation3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:914
    NAME ( 'suVisibAffilAddress1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:915
    NAME ( 'suVisibAffilAddress2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:916
    NAME ( 'suVisibAffilAddress3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:917
    NAME ( 'suVisibAffilPhone1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:918
    NAME ( 'suVisibAffilPhone2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:919
    NAME ( 'suVisibAffilPhone3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:920
    NAME ( 'suVisibAffilFax1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:921
    NAME ( 'suVisibAffilFax2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:922
    NAME ( 'suVisibAffilFax3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:923
    NAME ( 'suVisibAffilFax4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:924
    NAME ( 'suVisibAffilFax5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:925
    NAME ( 'suVisibTelephoneNumber' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:926
    NAME ( 'suVisibPermanentAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:927
    NAME ( 'suVisibLocalPhone' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:928
    NAME ( 'suVisibLocalAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:929
    NAME ( 'suVisibMailAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:930
    NAME ( 'suVisibPermanentPhone' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:931
    NAME ( 'suVisibAffiliation4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:932
    NAME ( 'suVisibAffiliation5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:933
    NAME ( 'suVisibAffilAddress4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:934
    NAME ( 'suVisibAffilAddress5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:935
    NAME ( 'suVisibAffilPhone4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:936
    NAME ( 'suVisibAffilPhone5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:941
    NAME ( 'suVisibMailCode' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:942
    NAME ( 'suVisibStreet' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:943
    NAME ( 'suVisibInternalPager' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:945
    NAME ( 'suVisibAffilInternalPager' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:946
    NAME ( 'suVisibAffilMobile' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype: ( StanfordLDAPattributeType:947
    NAME ( 'suVisibAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype: ( StanfordLDAPattributeType:948
    NAME ( 'suVisibEmailPager' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:955
    NAME ( 'suVisibAffilPager' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:2000
    NAME ( 'suLocalAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:2001
    NAME ( 'suPermanentAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:2002
    NAME ( 'suMailAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:2003
    NAME ( 'suResidenceRoom' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:3000
    NAME ( 'suLocalPhone' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:3001
    NAME ( 'suPermanentPhone' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:3002
    NAME ( 'suResidencePhone' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4000
    NAME ( 'suGwAffiliation1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4001
    NAME ( 'suGwAffiliation2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4002
    NAME ( 'suGwAffiliation3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4003
    NAME ( 'suGwAffiliation4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4004
    NAME ( 'suGwAffiliation5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4030
    NAME ( 'suGwAffilAddress1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4031
    NAME ( 'suGwAffilAddress2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4032
    NAME ( 'suGwAffilAddress3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4033
    NAME ( 'suGwAffilAddress4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4034
    NAME ( 'suGwAffilAddress5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4040
    NAME ( 'suGwAffilMailCode1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4041
    NAME ( 'suGwAffilMailCode2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4042
    NAME ( 'suGwAffilMailCode3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4043
    NAME ( 'suGwAffilMailCode4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4044
    NAME ( 'suGwAffilMailCode5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4050
    NAME ( 'suGwAffilPhone1' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4051
    NAME ( 'suGwAffilPhone2' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4052
    NAME ( 'suGwAffilPhone3' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4053
    NAME ( 'suGwAffilPhone4' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4054
    NAME ( 'suGwAffilPhone5' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4060
    NAME ( 'suGwAffilFax1' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4061
    NAME ( 'suGwAffilFax2' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4062
    NAME ( 'suGwAffilFax3' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4063
    NAME ( 'suGwAffilFax4' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4064
    NAME ( 'suGwAffilFax5' )
    EQUALITY telephoneNumberMatch
    SUBSTR telephoneNumberSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.50 )

attributetype ( StanfordLDAPattributeType:4070
    NAME ( 'suGwAffilDate1' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4071
    NAME ( 'suGwAffilDate2' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4072
    NAME ( 'suGwAffilDate3' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4073
    NAME ( 'suGwAffilDate4' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4074
    NAME ( 'suGwAffilDate5' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4075
    NAME ( 'suGwAffilCode1' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4076
    NAME ( 'suGwAffilCode2' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4077
    NAME ( 'suGwAffilCode3' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4078
    NAME ( 'suGwAffilCode4' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4079
    NAME ( 'suGwAffilCode5' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:4080
    NAME ( 'suGwAffilInternalPager' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4081
    NAME ( 'suGwAffilMobile' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4082
    NAME ( 'suGwAffilPager' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4101
    NAME ( 'suGwAffiliation' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:4102
    NAME ( 'suGwAffilCode' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:9998
    NAME ( 'suResidenceRequiredAttribute' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

objectclass ( StanfordLDAPobjectClass:1 NAME 'suPerson'
    DESC 'Stanford University Person Description'
    SUP ( inetOrgPerson )
    STRUCTURAL
    MUST ( displayName $
           suCN $
           suDisplayNameLF $
           suGeneralID $
           suRegID $
           suRegisteredName $
           suRegisteredNameLF $
           suSN $
           suUniqueIdentifier )
    MAY ( generationQualifier $
          krb5PrincipalName $
          personalTitle $
          suADPrimaryEmail $
          suAffiliation $
          suAffilJobCode $
          suAffilJobDescription $
          suAffilStandardHours $
          suCardNumber $
          suDisplayAffiliation $
          suDisplayNameFirst $
          suDisplayNameLast $
          suDisplayNameMiddle $
          suDisplayNamePrefix $
          suDisplayNameSuffix $
          suEmailPager $
          suFacultyAppointment $
          suFacultyAppointmentShort $
          suGALSyncDate $
          suGivenName $
          suGwAffilAddress1 $
          suGwAffilAddress2 $
          suGwAffilAddress3 $
          suGwAffilAddress4 $
          suGwAffilAddress5 $
          suGwAffilCode $
          suGwAffilCode1 $
          suGwAffilCode2 $
          suGwAffilCode3 $
          suGwAffilCode4 $
          suGwAffilCode5 $
          suGwAffilDate1 $
          suGwAffilDate2 $
          suGwAffilDate3 $
          suGwAffilDate4 $
          suGwAffilDate5 $
          suGwAffilFax1 $
          suGwAffilFax2 $
          suGwAffilFax3 $
          suGwAffilFax4 $
          suGwAffilFax5 $
          suGwAffiliation $
          suGwAffiliation1 $
          suGwAffiliation2 $
          suGwAffiliation3 $
          suGwAffiliation4 $
          suGwAffiliation5 $
          suGwAffilInternalPager $
          suGwAffilMailCode1 $
          suGwAffilMailCode2 $
          suGwAffilMailCode3 $
          suGwAffilMailCode4 $
          suGwAffilMailCode5 $
          suGwAffilMobile $
          suGwAffilPager $
          suGwAffilPhone1 $
          suGwAffilPhone2 $
          suGwAffilPhone3 $
          suGwAffilPhone4 $
          suGwAffilPhone5 $
          suLocalAddress $
          suLocalPhone $
          suMailAddress $
          suMailCode $
          suOtherName $
          suOU $
          suPermanentAddress $
          suPermanentPhone $
          suPrimaryOrganizationID $
          suPrimaryOrganizationName $
          suPrivilegeGroup $
          suProfile $
          suProxyCardNumber $
          suSearchID $
          suStanfordEndDate $
          suStudentType $
          suSunetID $
          suUnivID $
          suVisibAffilAddress1 $
          suVisibAffilAddress2 $
          suVisibAffilAddress3 $
          suVisibAffilAddress4 $
          suVisibAffilAddress5 $
          suVisibAffiliation1 $
          suVisibAffiliation2 $
          suVisibAffiliation3 $
          suVisibAffiliation4 $
          suVisibAffiliation5 $
          suVisibAffilFax1 $
          suVisibAffilFax2 $
          suVisibAffilFax3 $
          suVisibAffilFax4 $
          suVisibAffilFax5 $
          suVisibAffilInternalPager $
          suVisibAffilMobile $
          suVisibAffilPager $
          suVisibAffilPhone1 $
          suVisibAffilPhone2 $
          suVisibAffilPhone3 $
          suVisibAffilPhone4 $
          suVisibAffilPhone5 $
          suVisibEmail $
          suVisibFacsimileTelephoneNumber $
          suVisibHomeAddress $
          suVisibHomePage $
          suVisibHomePhone $
          suVisibIdentity $
          suVisibLocalAddress $
          suVisibLocalPhone $
          suVisibMailAddress $
          suVisibMailCode $
          suVisibMobilePhone $
          suVisibPagerEmail $
          suVisibPagerPhone $
          suVisibPermanentAddress $
          suVisibPermanentPhone $
          suVisibProfile $
          suVisibStreet $
          suVisibSunetID $
          suVisibTelephoneNumber ) )

objectclass ( StanfordLDAPobjectClass:310 NAME 'suCampusResident'
    DESC 'Stanford University On Campus Resident'
    AUXILIARY
    MUST ( suResidenceRequiredAttribute )
    MAY ( suResidenceCode $
          suResidenceName $
          suResidenceRoom $
          suResidencePhone $
          suResidenceTSO )
    )

objectclass ( StanfordLDAPobjectClass:460 NAME 'suAffiliation'
    DESC 'Stanford University Affiliation Properties'
    STRUCTURAL
    MUST ( cn $ suAffiliationType )
    MAY (  description
    $ facsimileTelephoneNumber
    $ friendlyCountryName
    $ localityName
    $ mobile
    $ ou
    $ pager
    $ postalCode
    $ stateOrProvinceName
    $ street
    $ suDepartment
    $ suEmailPager
    $ suInternalPager
    $ suJobCode
    $ suJobDescription
    $ suMailCode
    $ suRelationship
    $ suStandardHours
    $ suStanfordEndDate
    $ suStanfordStartDate
    $ suVisibAddress
    $ suVisibEmailPager
    $ suVisibFacsimileTelephoneNumber
    $ suVisibInternalPager
    $ suVisibMobilePhone
    $ suVisibPagerPhone
    $ suVisibStreet
    $ suVisibTelephoneNumber
    $ title
    $ telephoneNumber ) )

objectclass ( StanfordLDAPobjectClass:461 NAME 'suPersonSet'
    DESC 'Stanford Person Properties Container'
    STRUCTURAL
    MUST ( suRegID )
    MAY ( uid ) )
