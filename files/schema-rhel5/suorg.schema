attributetype ( StanfordLDAPattributeType:5000 NAME ( 'suAcadID' )
	EQUALITY caseIgnoreMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5001 NAME ( 'suAdminID' )
	EQUALITY caseIgnoreMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5002 NAME ( 'suListedName' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5003 NAME ( 'suSystemID' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5004 NAME ( 'suChildOrgRegID' )
    EQUALITY caseIgnoreIA5Match
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )

attributetype ( StanfordLDAPattributeType:5005 NAME ( 'suParentOrgRegID' )
    EQUALITY caseIgnoreIA5Match
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5006 NAME ( 'suOrgNote' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:5007 NAME ( 'suDisplayName30' )
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5008 NAME ( 'suPreviousName' )
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} )

attributetype ( StanfordLDAPattributeType:5009 NAME ( 'suPrintName' )
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5010 NAME ( 'suPrintSection' )
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 )

attributetype ( StanfordLDAPattributeType:5011 NAME ( 'suOrgContactStanford' )
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:5012 NAME ( 'suOrgContactWorld' )
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:5013 NAME ( 'suOrgContactLabelStanford' )
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:5016 NAME ( 'suOrgContactLabelWorld' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:5014 NAME ( 'suOrgStatus' )
	EQUALITY caseIgnoreMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:59 NAME ( 'suAcronym' )
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} SINGLE-VALUE )

attributetype ( StanfordLDAPattributeType:5015 NAME ( 'suOrgWorkgroup' )
    EQUALITY distinguishedNameMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.12 )

attributetype ( StanfordLDAPattributeType:5017 NAME ( 'suGwOrgAddress' )
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:5018 NAME ( 'suSeeAlsoOrgRegID' )
    EQUALITY caseIgnoreIA5Match
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )

attributetype ( StanfordLDAPattributeType:5019 NAME ( 'suSearchName' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

attributetype ( StanfordLDAPattributeType:5020 NAME ( 'suOrgLocation' )
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

objectclass ( StanfordLDAPobjectClass:400 NAME 'suOrganization' 
	DESC 'Stanford University Organization Description'
	SUP ( organizationalUnit )
	STRUCTURAL
    MUST ( suRegID $ suRegisteredName $ displayName $ suParentOrgRegID $ 
            suUniqueIdentifier $ suOrgStatus $ suGeneralID $
            suVisibIdentity $ suDisplayName30 $ suSearchName )
	MAY ( suAcadID $ suAdminID $ suChildOrgRegID $ suSystemID $ 
            suListedName $ telephoneNumber $ facsimileTelephoneNumber $ 
            street $ postalAddress $ suMailCode $ labeledURI $ mail $ 
            suPrintSection $ suOrgNote $ suPrintName $ suPreviousName $ 
            suOrgContactStanford $ suOrgContactWorld $ suAcronym $ 
            suOrgContactLabelStanford $ suOrgContactLabelWorld $ 
            suOrgWorkgroup $ suOtherName $ suSeeAlsoOrgRegID $ 
            suGwOrgAddress $ suOrgLocation )
	)

### Temporary objectClass definition so that suParentOrgRegID and suDisplayName30 are optional, until the data is fixed.
#objectclass ( StanfordLDAPobjectClass:400 NAME 'suOrganization' 
#	DESC 'Stanford University Organization Description'
#	SUP ( organizationalUnit )
#	STRUCTURAL
#		MUST ( suRegID $ suRegisteredName $ displayName $ suUniqueIdentifier $ suOrgStatus $ suGeneralID $ suVisibIdentity )
#		MAY ( suAcadID $ suAdminID $ suChildOrgRegID $ suSystemID $ suListedName $
#			telephoneNumber $ facsimileTelephoneNumber $ street $ postalAddress $
#			suMailCode $ labeledURI $ mail $ suPrintSection $ suOrgNote $ suPrintName $
#			suPreviousName $ suOrgContactStanford $ suOrgContactWorld $ suAcronym $
#			suOrgContactLabelStanford $ suOrgContactLabelWorld $ suOrgWorkgroup $
#			suOtherName $ suSeeAlsoOrgRegID $ suGwOrgAddress $ suParentOrgRegID $ suDisplayName30 )
#	)

objectClass ( StanfordLDAPobjectClass:401 NAME 'suUberOrganization'
	DESC 'Stanford University Top Level Organization'
	SUP ( organization )
	STRUCTURAL
	MUST ( suRegID $ suRegisteredName $ displayName $ suUniqueIdentifier $ 
            suOrgStatus $ suGeneralID $ suVisibIdentity $ suDisplayName30 $ 
            suChildOrgRegID )
	)
